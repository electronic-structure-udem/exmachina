# ExMachina

The code for the exchange hole models using neural networks.
There are two input example to optimize the NN parameters (currently it's doing one step with a learning rate of 0, so it only calculates the MSE on the constraints):
- opt_NN_alpha for the ExMc alpha model or ExMc if alpha is set to none
- opt_NN.py for the factor described in the article.

# Note
The ExMc.pt was optimized an older version of the code, where the exchange hole was positive. Thus, a negative sign for the curvature has to be used for this model. The ExMc_epsilon_BR.py also used a positive hole, however since the curvature is contrainted to 0, the optimization part of the code does not change.
# TODO
- Cleaning up the code, using "None" in the input is unnecessary
