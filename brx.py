import numpy as np
import torch
import B89_curvature


def jBR(InputTensor,curv,n_eps,n_y,device):
    """
    To create a tf tensor a the BR X hole for various curvature
    """
    #generate hole on curvature grid
    n_c=curv.size
    y_grid=InputTensor[0:n_y*n_c:n_c,0].numpy()
    jBRyc=np.empty([n_y,n_c])
    # get BR parameters for the c grid
    for i in range(n_c):
        BRPara=B89_curvature.B89_param_tot(curv[i])
        jBRyc[:,i]=-jx_br_tot(BRPara[0],BRPara[1],BRPara[2],y_grid)
        jBRyc[0,i]=-0.5

    OneC=np.reshape(jBRyc,n_c*n_y)
    OneC_tf=torch.from_numpy(OneC)

    xBR_tf = torch.zeros((n_y*n_c*n_eps,1)).to(device).detach()
    #print("hole:",OneC_tf,xBR_tf)
    shift=0
    for j in range(n_eps):
        for k in range(n_y*n_c):
            xBR_tf[k+shift,0]=OneC_tf[k]
        shift=shift+n_c*n_y

    return xBR_tf


def jx_br(a,b,c,y):
    """
    Calculate the BR x hole 
    """
    with np.errstate(divide='ignore',invalid="ignore"):
        return c/(2.*a**2*b*y)*((a*np.abs(b-y)+1)*np.exp(-a*np.abs(b-y))-\
            (a*np.abs(b+y)+1)*np.exp(-a*np.abs(b+y)))

def jx_br_tot(a,b,c,y):
    """
    Caclulate the total reduced spin unpolarized X hole
    """
    return 0.5*jx_br(a,b,c,0.5**(1/3)*y)
