from NN_rhoxnn import RhoxNN,Net,Net_factor
import torch
import numpy as np
from brx import jBR
#grid info
grid_type="gauleg" #gauss legendre and uniform are implemented
degree=300

#params to optimize
curv=np.arange(3.9,-5.4,-0.5)#
alpha=None# None if not desired WARNING NOT YET IMPLEMENTED FOR FACTOR
efx=np.array([1.0,0.95,0.9,0.85,0.8,0.75,1.1,1.05,1.15,1.2])# None if not desired THE FIRST VALUE MUST BE ONE
dim = (curv is not None) + (efx is not None) +1 #dimensions
#build the rhox class
factor=True # if the NN is a factor multiplying a hole
load_file="ExMc_epsilon_BR.pt"# name of file to load
device="cpu"
num_threads=6
rhox=RhoxNN(dim,load_file,factor,grid_type=grid_type,degree=degree,curv=curv,efx=efx,
            dev=device,alpha=alpha,num_threads=num_threads)
# load a previous model to optimize a factor
previous_model=jBR(rhox.inp[:,:2].to(device).detach(),curv,efx.size,degree+1,device)
#previous_model=None

#NN optimisation , uses Adam algorithm
opt = True
n_epoch=1
learning_rate=0.0000
sigma = np.arange(10000,10000+n_epoch)
opt_norm=True
opt_efx=True
opt_ontop=True
ontop=1. #value of on-top condition
opt_curv=True
opt_cusp=True
cusp=0. # value of the first derivative at origin
save_file="test3.pt"

# optiomisation part
if opt==True:
    rhox.opt_nn(previous_model=previous_model,n_epoch=n_epoch,lr=learning_rate,
                sigma=sigma,opt_norm=opt_norm,opt_efx=opt_efx,
                opt_ontop=opt_ontop,ontop=ontop,opt_curv=opt_curv,
                opt_cusp=opt_cusp,cusp=cusp,save_file=save_file)
