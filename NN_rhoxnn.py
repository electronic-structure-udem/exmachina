import torch.nn as nn 
import torch.nn.functional as F 
import numpy as np
import torch 
import matplotlib.pyplot as plt

#description of the NN
class Net(nn.Module):
    def __init__(self,N_inputs,hidden=100):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(N_inputs, hidden)
        self.fc2 = nn.Linear(hidden, hidden)
        self.fc3 = nn.Linear(hidden, hidden)
        self.fc4 = nn.Linear(hidden, 1)

    def forward(self, x):
        g1=F.softplus(self.fc1(x))
        g2=F.softplus(self.fc2(g1))
        g3=F.softplus(self.fc3(g2))
        g4=F.softplus(self.fc4(g3))
        return -g4

#description of the NN for the factor
class Net_factor(nn.Module):
    def __init__(self,N_inputs,hidden=100):
        super(Net_factor, self).__init__()
        self.fc1 = nn.Linear(N_inputs, hidden)
        self.fc2 = nn.Linear(hidden, hidden)
        self.fc3 = nn.Linear(hidden, hidden)
        self.fc4 = nn.Linear(hidden, 1)

    def forward(self, x):
        g1=F.tanh(self.fc1(x))
        g2=F.tanh(self.fc2(g1))
        g3=F.tanh(self.fc3(g2))
        g4=F.softplus(self.fc4(g3))
        return g4

class RhoxNN:
    def __init__(self,dim,load_file=None,factor=False,grid_type="gauleg",degree=300,a=0.,b=25.,curv=None, 
                            efx=None,num_threads=1,dev="cpu",alpha=None):
        """
        Parameters:
            dim(int): dimension of the input tensor
            load_file(string):file name to load NN parameters if desired
            factor(bool): if it's a factor that multiplies a previously converged NN
            load_file(string): to load a previous NN
            grid_type (str): gauss legendre (gauleg) or uniform integration grid
            degree (int): number of integration points
            a (float): first integration point
            b (float) : last integration point
            curv (array of float): curvature values to reproduce
            efx(array of float): exchange energy densities relative to LSD values to reproduce
            num_threads(int)
            dev(string):cpu or gpu
            alpha: alpha value for the modified curvature constraint
        """
        #for the ingrataion grid TODO, uniform
        torch.set_num_threads(num_threads)
        device = torch.device(dev)  
        self.alpha=alpha
        self.a=a
        self.b=b
        self.degree=degree
        self.dim=dim
        self.factor=factor
        if self.factor==True:
            self.model = Net_factor(dim).to(dev)
        else:
            self.model=Net(dim).to(dev)
        if load_file is not None:
            self.model.load_state_dict(torch.load(load_file,map_location=dev))

        if grid_type=="gauleg":
            gauleg_x,gauleg_w=np.polynomial.legendre.leggauss(degree)
            gauleg_x= (b-a)/2.*gauleg_x+(a+b)/2.
            x=np.concatenate(([0.],gauleg_x),axis=None) # we add the value of 0 at the origin with a weight of 0
            gauleg_w=np.concatenate(([0.],gauleg_w),axis=None)
            self.weight=torch.unsqueeze(torch.tensor((b-a)/2.*gauleg_w),dim=1)
            self.weight = self.weight.to(device)
        if grid_type=="uniform":
            x = np.linspace(a, b, degree+1)
            self.weight = (b-a)/degree

        # for curvature
        self.curv=curv
        if curv is not None:
            self.N_curv = curv.size
        else:
            self.N_curv=1
        
        #for efx
        self.efx=efx
        if efx is not None:
            self.N_efx=efx.size
        else:
            self.N_efx=1
        #for the input tensor
        if (efx is not None)  and (curv is not None):
            self.inp = torch.tensor(np.array(np.meshgrid(x, 
                         curv,efx)).T.reshape(-1, 3),requires_grad=True).float().to(dev)
        elif (efx is not None)  and (curv is None):
            self.inp = torch.tensor(np.array(np.meshgrid(x, 
                         efx)).T.reshape(-1, 2),requires_grad=True).float().to(dev)
        elif (efx is None)  and (curv is not None):
            self.inp= torch.tensor(np.array(np.meshgrid(x, 
                         curv)).T.reshape(-1, 2),requires_grad=True).float().to(dev)
        else:
            self.inp = torch.tensor(x.T.reshape(-1, 1),requires_grad=True).float().to(dev)
        self.inp = self.inp.to(device)

###### CONSTRAINTS FUNCTIONS #################
    def error_norm_factor(self,pred,previous_model):
        """
        Calculate the total error for the normalisation condition of a factor multiplying a previous exchange hole.

        Parameters:
        pred (tensor): current prediction for the factor
        previous_model(tensor): a previous model to multiply the factor
        """
        error=0.
        #loop on each epx and curvature to optimize
        for j in range(self.first_efx,self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)
                error+=(-1.-4./(3.*np.pi)*torch.sum(self.weight*previous_model[first:last:(self.N_curv)]*
                                    self.inp[first:last:(self.N_curv),0].reshape(-1,1)**2*pred[first:last:(self.N_curv)]))**2
        return error

    def error_norm(self,pred):
        """
        Calculate the total squared error for the normalisation condition of a predicted exchange hole.

        Parameters:
        pred (tensor): prediction for the exchange hole
        """
        error=0.
        #loop on each epx and curvature to optimize
        for j in range(self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)
                error+=(-1.-4./(3.*np.pi)*torch.sum(self.weight*
                                    self.inp[first:last:(self.N_curv),0].reshape(-1,1)**2*pred[first:last:(self.N_curv)]))**2
        return error

    def error_efx_factor(self,pred,previous_model):
        """
        Calculate the total squared error on the constraint to reproduce an enhancement factor, using a factor multiplying
        a previously converged X-hole.

        Parameters:
        pred (tensor): current prediction for the factor
        previous_model(tensor): previous model to multiply the hole
        """
        error=0.
        #loop on each epx and curvature to optimize
        for j in range(self.first_efx,self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)
                previous_efx_mat=self.weight*self.inp[first:last:(self.N_curv),0].reshape(-1,1)*\
                                previous_model[first:last:(self.N_curv)]
                previous_efx=8./9.*torch.sum(previous_efx_mat)
                error+=( self.inp[first,2]*previous_efx-8./9.*(torch.sum(previous_efx_mat*pred[first:last:(self.N_curv)])))**2 
        return error

    def error_f1(self,pred):
        """
        Deviation of the factor from 1
        """

        error=0.

        for j in range(self.first_efx,self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)

                error+=torch.sum(self.weight*( pred[first:last:(self.N_curv)]-1.)**2)
        return error

    def error_efx1(self,pred):
        """
        If the enhancement factor is one, we calculate the error to have the factor=1 for all y inputs.
        """
        error=0.
        #loop on each curvature for the first epx
        for i in range(self.N_curv):
            first=i
            last=first+self.N_curv*(self.degree+1)
            error+=torch.sum(self.weight*(pred[first:last:(self.N_curv)]-1)**2)
        return error

    def error_efx(self,pred):
        """
        Calculate the total squared error on the constraint to reproduce enhancement factor for a X hole

        Parameters:
        pred (tensor): current prediction for the X hole
        """
        error=0.
        #loop on each epx and curvature to optimize
        for j in range(self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)
                error+=(self.inp[first,2]-8./9.*(torch.sum(self.weight*self.inp[first:last:(self.N_curv),0].reshape(-1,1)*
                pred[first:last:(self.N_curv)])))**2 
        return error

    def error_ontop(self,pred,ontop=1.):
        """
        Calculate the total squared error for the on-top condition
        Parameters:
            pred(tensor): current prediction for the X hole or factor
            ontop(float): desired on-top value
        """
        e_ontop=0.
        for j in range(self.first_efx,self.N_efx):
            first=j*self.N_curv*(self.degree+1)
            last=first+self.N_curv
            e_ontop += torch.sum((pred[first:last]-ontop)**2)
        return e_ontop

    def error_cusp(self,pred,cusp=0.):
        """
        Calculate the total squared error on the first derivative at the origin of the X hole or factor

        Parameters:
        pred(tensor): current prediction for the X hole or factor
        cusp(float): value for first derivative at origin
        """
        e_cusp=0.
        for j in range(self.first_efx,self.N_efx):
            first=j*self.N_curv*(self.degree+1)
            last=first+self.N_curv
            e_cusp+=torch.sum((self.g_inp[first:last,0]-cusp)**2)  
        return e_cusp

    def error_curv_alpha(self,pred):
        """
        Calculate the total squared error on the second derivative from to origin to a range defined
        by a gaussian with a parameter alpha for the X hole
        Parameters:
            pred(tensor):current prediction for the X hole or factor
        """
        e_curv=0.
        for j in range(self.first_efx,self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)
                e_curv+=torch.sum(self.weight*(torch.exp(-self.alpha*self.inp[first:last:(self.N_curv),0].reshape(-1,1)**2)*
                    (self.inp[first:last:self.N_curv,1].reshape(-1,1)-self.g2_inp[first:last:self.N_curv,0].reshape(-1,1))**2))
                #e_curv+=torch.sum((self.inp[:self.N_curv,1]-self.g2_inp[first:last,0])**2)
        return e_curv/(np.sqrt(np.pi)/(2*np.sqrt(self.alpha)))
    
    def error_curv(self,pred):
        """
        Calculate the total squared error on the second derivative at the origin for the X hole
        Parameters:
            pred(tensor):current prediction for the X hole or factor
        """
        e_curv=0.
        for j in range(self.first_efx,self.N_efx):
            first=j*self.N_curv*(self.degree+1)
            last=first+self.N_curv
            e_curv+=torch.sum((self.inp[:self.N_curv,1]-self.g2_inp[first:last,0])**2)
        return e_curv

    def error_curv_factor(self,pred,curv=0.):
        """
        Calculate the total squared error on the second derivative at the origin for a factor
        Parameters:
            pred(tensor):current prediction for the factor
        """
        e_curv=0.
        for j in range(self.first_efx,self.N_efx):
            first=j*self.N_curv*(self.degree+1)
            last=first+self.N_curv
            e_curv+=torch.sum((curv-self.g2_inp[first:last,0])**2)
        return e_curv

##### entropy
    def entropy_factor(self,pred,previous_model):
        """
        Calculate the entropy for the factor multiplying a previously converged X-hole
        Parameters:
        pred (tensor): current prediction for the factor
        previous_model(tensor): a previous model to multiply the factor
        """
        lnp = torch.log(-pred*previous_model)
        entropy_tot =0.
        for j in range(self.first_efx,self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)
                entropy_tot+=torch.sum(self.weight*previous_model[first:last:(self.N_curv)]*
                            pred[first:last:(self.N_curv)]*lnp[first:last:(self.N_curv)])
        return entropy_tot

    def entropy(self,pred):
        """
        Calculate the entropy for a reduced X-hole
        Parameters:
        pred (tensor): current prediction for the factor
        """
        lnp = torch.log(-pred+1e-40)# add a small number so it's non zero
        entropy_tot =0.
        for j in range(self.N_efx):
            for i in range(self.N_curv):
                first=i+self.N_curv*(self.degree+1)*j 
                last=first+self.N_curv*(self.degree+1)
                entropy_tot+=torch.sum(self.weight*pred[first:last:(self.N_curv)]*lnp[first:last:(self.N_curv)])
        return entropy_tot

    ### derivative
    def calc_deriv(self,pred):
        """
        Calculate the first and second derivative of the neural network
        Parameters:
            pred(tensor): current prediction for the factor or X hole
        """
        self.g_inp,=torch.autograd.grad(pred,self.inp,create_graph=True,grad_outputs=torch.ones_like(pred))
        self.g2_inp, = torch.autograd.grad(self.g_inp, self.inp, create_graph=True,grad_outputs=torch.ones_like(self.g_inp))

    ###loss function
    def entropy_loss(self,pred,sigma,opt_norm=True,opt_efx=True,
                opt_ontop=True,ontop=1.,opt_curv=True,opt_cusp=True,cusp=0.):
        """
        Calculate the loss function based on the maximization of the entropy, while minimizing the errors
        on the constraints.
        Currently, the penalty method is used.
        Parameters:
            pred(tensor): current prediciton for the X hole
            sigma(float): penalty for the constraints
            opt_norm(bool): True to consider the normalisation condition
            opt_efx(bool) True to reproduce an enhancement factor
            opt_ontop(bool): True to consider the ontop condition
            ontop(float): value for the ontop condition
            opt_curv(bool): True to consider the curvature condition
            opt_cusp(bool): True to consider the cusp condition

        """
        # calculate derivative
        if opt_curv==True or opt_cusp==True:
            self.calc_deriv(pred)
        ###constraints
        e_norm =0.
        e_efx=0.
        e_ontop=0.
        e_curv=0.
        e_cusp=0.
        if opt_norm==True:
            e_norm=self.error_norm(pred)
        if opt_efx==True:
            e_efx=self.error_efx(pred)
        if opt_ontop==True:
            e_ontop=self.error_ontop(pred,ontop)
        if opt_curv==True and self.alpha is not None:
            e_curv=self.error_curv_alpha(pred)
        if opt_curv==True and self.alpha is None:
            e_curv=self.error_curv(pred)
        if opt_cusp==True:
            e_cusp=self.error_cusp(pred,cusp)
        #penalty method

        total_error =sigma*e_ontop+sigma*e_norm+sigma*e_efx +sigma*e_cusp+sigma*e_curv
        ###loss function
        S = -self.entropy(pred)
        L=-S +total_error
        #print the errors for the last step
        if(self.step==(self.n_epoch-1)):
            print("errors")
            print("norm:",e_norm/(self.N_efx*self.N_curv))
            print("efx:",e_efx/(self.N_efx*self.N_curv))
            print("ontop:",e_ontop/(self.N_efx*self.N_curv))
            print("curv:",e_curv/(self.N_efx*self.N_curv))
            print("cusp:",e_cusp/(self.N_efx*self.N_curv))
        return L

    def f1_loss_factor(self,pred,previous_model,sigma,opt_norm=True,opt_efx=True,
                opt_ontop=True,ontop=1.,opt_curv=True,opt_cusp=True,cusp=0.):
        """
        Calculate the loss function based on the minimisation of the deviation from 1, while minimizing the errors
        on the constraints.
        Currently, the penalty method is used.
        Function the a factor multiplying a a previous model
        Parameters:
            pred(tensor): current prediciton for the X hole
            previous_model(tensor): previous model to multiply the hole
            sigma(float): penalty for the constraints
            opt_norm(bool): True to consider the normalisation condition
            opt_efx(bool) True to reproduce an exchange energy density
            opt_ontop(bool): True to consider the ontop condition
            ontop(float): value for the ontop condition
            opt_curv(bool): True to consider the curvature condition
            opt_cusp(bool): True to consider the cusp condition

        """
        # calculate derivative
        if opt_curv==True or opt_cusp==True:
            self.calc_deriv(pred)
        ###constraints
        e_norm =0.
        e_efx=0.
        e_ontop=0.
        e_curv=0.
        e_cusp=0.
        e_efx1=0
        if opt_norm==True:
            e_norm=self.error_norm_factor(pred,previous_model)
        if opt_efx==True:
            e_efx1 =self.error_efx1(pred)# for efx=1, factor=1
            e_efx=self.error_efx_factor(pred,previous_model)
        if opt_ontop==True:
            e_ontop=self.error_ontop(pred,ontop)
        if opt_curv==True:
            e_curv=self.error_curv_factor(pred)
        if opt_cusp==True:
            e_cusp=self.error_cusp(pred,cusp)
        total_error =sigma*e_ontop+sigma*e_norm+sigma*e_efx +sigma*e_cusp+sigma*e_curv+sigma*e_efx1
        ###loss function
        L=self.error_f1(pred)+total_error
        if(self.step==(self.n_epoch-1)):
            print("errors")
            print("norm:",e_norm/(self.N_efx*self.N_curv))
            print("efx:",e_efx/(self.N_efx*self.N_curv))
            print("ontop:",e_ontop/(self.N_efx*self.N_curv))
            print("curv:",e_curv/(self.N_efx*self.N_curv))
            print("cusp:",e_cusp/(self.N_efx*self.N_curv))
        return L

    def opt_nn(self,n_epoch=1000,lr=0.001,sigma=np.arange(0,1000),previous_model=None,opt_norm=True,opt_efx=True,
            opt_ontop=True,ontop=1.,opt_curv=True,opt_cusp=True,cusp=0.,save_file=None):
        """
        Function to optimize the neural network parameters, by entropy maximation under constraints using the penalty method.

        Parameters:
            n_epoch(int): Number of epochs
            lr(float): learning rate
            sigma(array): penalty for the constraints
            previous_model(tensor): previously model to multiply a factor
            opt_norm(bool): True to consider the normalisation condition
            opt_efx(bool) True to reproduce an exchange energy density
            opt_ontop(bool): True to consider the ontop condition
            ontop(float): value for the ontop condition
            opt_curv(bool): True to consider the curvature condition
            opt_cusp(bool): True to consider the cusp condition
            save_file(string): file name to save the converged NN

        """
        optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)# can be changed to another algorithm
        self.n_epoch=n_epoch
        #loop over the epoch
        self.opt_efx=opt_efx
        self.first_efx=0
        if (self.factor==True) and (opt_efx==True):
            self.first_efx=1
        for self.step in range(self.n_epoch):
            sigma_step=sigma[self.step]
            pred=self.model(self.inp)
            if previous_model==None:
                loss=self.entropy_loss(pred,sigma_step,opt_norm,opt_efx,opt_ontop,ontop,opt_curv,opt_cusp,cusp)
            else:
                loss=self.f1_loss_factor(pred,previous_model,sigma_step,
                            opt_norm,opt_efx,opt_ontop,ontop,opt_curv,opt_cusp,cusp)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            print("loss",self.step,loss)
        if save_file is not None:
            torch.save(self.model.state_dict(), save_file)
        return self.model


